from django.shortcuts import render, get_object_or_404
from .models import Project
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "project": projects,
    }
    return render(request, "list.html", context)


@login_required
def show_details(request, id):
    projects = get_object_or_404(Project, id=id)
    context = {
        "project_object": projects,
    }
    return render(request, "details.html", context)
